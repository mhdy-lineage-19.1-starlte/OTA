#!/bin/sh

# Generate OTA json
# Usage:
#   ota.sh IMAGE_PATH URL

PROGRAM_NAME="$(basename "$0")"

EXIT_SUCCESS=0
EXIT_FAILURE=1

LINEAGE_VERSION=19.1

image="$1"
url="$2"

[ -z "$image" ] \
  && printf "usage: $PROGRAM_NAME image\n" \
  && exit $EXIT_FAILURE

cat << EOF > starlte.json
{
  "response": [
    {
      "datetime": "$(date +%s)",
      "filename": "$(basename "$image")",
      "id": "$(sha256sum "$image" | cut -d ' ' -f 1)",
      "romtype": "UNOFFICIAL",
      "size": "$(du -b "$image" | tr '\t' ' ' | cut -d ' ' -f 1)",
      "url": "$url",
      "version": "$LINEAGE_VERSION"
    }
  ]
}
EOF

exit $EXIT_SUCCESS
